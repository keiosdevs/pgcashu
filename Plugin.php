<?php namespace Keios\PGCashU;

use Keios\PaymentGateway\Models\Settings as PaymentGatewaySettings;
use System\Classes\PluginBase;
use Event;

/**
 * PG-PGCashU Plugin Information File
 *
 * @package Keios\PGCashU
 */
class Plugin extends PluginBase
{
    public $require = [
        'Keios.PaymentGateway',
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'PG-CashU',
            'description' => 'keios.pgcashu::lang.labels.pluginDesc',
            'author'      => 'Keios',
            'icon'        => 'icon-cashu',
        ];
    }

    public function register()
    {
        Event::listen(
            'paymentgateway.booted',
            function () {
                /**
                 * @var \October\Rain\Config\Repository $config
                 */
                $config = $this->app['config'];
                $config->push('keios.paymentgateway.operators', 'Keios\PGCashU\Operators\CashU');
            }
        );

        Event::listen(
            'kernel.middleware.csrf',
            function ($middleware) {
                $middleware->whitelist('_paymentgateway/*');
            }
        );

        Event::listen(
            'backend.form.extendFields',
            function ($form) {

                if (!$form->model instanceof PaymentGatewaySettings) {
                    return;
                }

                if ($form->context !== 'general') {
                    return;
                }

                /**
                 * @var \Backend\Widgets\Form $form
                 */
                $form->addTabFields(
                    [
                        'cashu.general'       => [
                            'label' => 'keios.pgcashu::lang.settings.general',
                            'tab'   => 'keios.pgcashu::lang.settings.tab',
                            'type'  => 'section',
                        ],
                        'cashu.info'          => [
                            'type' => 'partial',
                            'path' => '$/keios/pgcashu/partials/_cashu_info.htm',
                            'tab'  => 'keios.pgcashu::lang.settings.tab',
                        ],
                        'cashu.ipn_url'       => [
                            'label' => 'keios.pgcashu::lang.settings.ipn_url',
                            'tab'   => 'keios.pgcashu::lang.settings.tab',
                            'type'  => 'partial',
                            'path'  => '$/keios/pgcashu/partials/url.htm',
                        ],
                        'cashu.mechantId'     => [
                            'label'   => 'keios.pgcashu::lang.settings.mechantId',
                            'tab'     => 'keios.pgcashu::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '',
                        ],
                        'cashu.encryptionKey' => [
                            'label'   => 'keios.pgcashu::lang.settings.encryptionKey',
                            'tab'     => 'keios.pgcashu::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '',
                        ],
                        'cashu.serviceName'   => [
                            'label'   => 'keios.pgcashu::lang.settings.serviceName',
                            'tab'     => 'keios.pgcashu::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '',
                        ],
                        'cashu.thankyou_page' => [
                            'label' => 'keios.pgcashu::lang.settings.thankyou_page',
                            'tab'   => 'keios.pgcashu::lang.settings.tab',
                        ],
                        'cashu.language'      => [
                            'label'   => 'keios.pgcashu::lang.settings.language',
                            'tab'     => 'keios.pgcashu::lang.settings.tab',
                            'type'    => 'dropdown',
                            'options' => [
                                'en' => 'English',
                            ],
                        ],
                        'cashu.testMode'      => [
                            'label' => 'keios.pgcashu::lang.settings.testMode',
                            'tab'   => 'keios.pgcashu::lang.settings.tab',
                            'type'  => 'switch',
                        ],
                    ]
                );
            }
        );
    }
}

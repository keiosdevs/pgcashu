<?php namespace Keios\PGCashU\Classes;

use Keios\PaymentGateway\Contracts\Orderable;
use Keios\PaymentGateway\ValueObjects\Details;
use Keios\PaymentGateway\ValueObjects\CreditCard;

/**
 * Class CashUProcessor
 * @package Keios\PGCashU\Classes
 */
class CashUProcessor
{

    /**
     * @param CashUPayment $object
     *
     * @return array
     */
    public function getRequestArray($object)
    {
        $array = [
            'merchant_id'  => $object->merchantId,
            'token'        => $object->token,
            'display_text' => $object->description,
            'currency'     => $object->currency,
            'amount'       => $object->amount,
            'language'     => $object->language,
            'session_id'   => $object->sessionId,
            'txt1'         => $object->hiddenDescription,
            'test_mode'    => $object->testMode,
            'service_name' => $object->serviceName,
        ];

        $this->validateRequestArray($array);

        return $array;
    }

    /**
     * @param array $array
     *
     * @throws \ValidationException
     */
    private function validateRequestArray(array $array)
    {
        $rules = [
            'merchant_id'  => 'required',
            'token'        => 'required',
            'display_text' => 'required',
            'currency'     => 'required',
            'amount'       => 'required',
            'language'     => 'required',
            'session_id'   => 'required',
            'test_mode'    => 'required',
        ];

        $validator = \Validator::make($array, $rules);

        if ($validator->fails()) {
            throw new \ValidationException($validator);
        }
    }

    /**
     * @param       $url
     * @param array $parameters
     *
     * @return mixed
     */
    public function executePostRequest($url, array $parameters)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameters));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }
}
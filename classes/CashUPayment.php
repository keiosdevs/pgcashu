<?php
/**
 * Created by Keios Solutions
 * User: Jakub Zych
 * Date: 2/12/16
 * Time: 12:24 PM
 */

namespace Keios\PGCashU\Classes;


use Illuminate\Support\Facades\Validator;

/**
 * Class CashUPayment
 * @package Keios\PGCashU\Classes
 */
class CashUPayment
{
    /**
     * @var string
     */
    public $merchantId;

    /**
     * @var string
     */
    public $token;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $currency;

    /**
     * @var float|integer
     */
    public $amount;

    /**
     * @var string
     */
    public $language;

    /**
     * @var string
     */
    public $sessionId;

    /**
     * @var string
     */
    public $hiddenDescription;

    /**
     * @var boolean
     */
    public $testMode;

    /**
     * @var string
     */
    public $serviceName;

   /**
    * @var string
    */
    public $hashsId;

}
<?php

return [
    'labels'    => [
        'pluginDesc' => 'CashU Integration Extension for Keios PaymentGateway',
    ],
    'operators' => [
        'cashu' => 'CashU',
    ],
    'settings'  => [
        'tab'                   => 'CashU',
        'general'               => 'General settings',
        'apiKey'                => 'You CashU ApiKey',
        'testMode'              => 'Use test mode',
        'ipn_url'               => 'Notification URL',
        'mechantId'             => 'Merchant ID',
        'encryptionKey'         => 'Encryption Key',
        'serviceName'           => 'Service Name',
        'thankyou_page'         => 'Thank you page',
        'thankyou_page_comment' => 'To redirect after succesful payment. Hash id will be attached under hashid get attribute',
        'language'              => 'Language',
    ],
    'info'      => [
        'header' => 'How to retrieve your CashU api key',
    ],
];
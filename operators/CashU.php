<?php namespace Keios\PGCashU\Operators;

use Finite\StatefulInterface;
use Keios\PaymentGateway\Contracts\Orderable;
use Keios\PaymentGateway\Support\HashIdsHelper;
use Keios\PaymentGateway\Support\OperatorUrlizer;
use Keios\PGCashU\Classes\CashUPayment;
use Keios\PGCashU\Classes\CashUProcessor;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keios\PaymentGateway\ValueObjects\PaymentResponse;
use Keios\PaymentGateway\Traits\SettingsDependent;
use Keios\PGCashU\Classes\CashUChargeMaker;
use Keios\PaymentGateway\Core\Operator;
use CashU\CashU as CashUApi;
use CashU\Charge;

/**
 * Class CashU
 *
 * @package Keios\PGCashU
 */
class CashU extends Operator implements StatefulInterface
{
    use SettingsDependent;

    /**
     *
     */
    const CREDIT_CARD_REQUIRED = false;

    /**
     * @var string
     */
    public static $operatorCode = 'keios.pgcashu::lang.operators.cashu';

    /**
     * @var string
     */
    public static $operatorLogoPath = '/plugins/keios/pgcashu/assets/img/cashu/logo.png';

    /**
     * @var string
     */
    public static $modeOfOperation = 'api';

    /**
     * @var string
     */
    protected static $apiUrl = 'https://www.cashu.com/cgi-bin/pcashu.cgi';

    /**
     * @var string
     */
    protected static $testApiUrl = 'https://sandbox.cashu.com/cgi-bin/pcashu.cgi';
    /**
     * @var array
     */
    public static $configFields = [];

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendPurchaseRequest()
    {
        $processor = new CashUProcessor();
        $chargeObject = $this->prepareCharge();
        $charge = $processor->getRequestArray($chargeObject);

        // this is so stupid, but CashU is even more stupid and doesn't send it back in failure redirects.
        \Session::put('payment_uuid', $chargeObject->sessionId);

        if ($chargeObject->testMode === "1") {
            return new PaymentResponse($this, self::$testApiUrl, null, $charge, false);
        }

//dd($this);
        return new PaymentResponse($this, self::$apiUrl, null, $charge, false);
        /*
                $internalRedirect = \URL::to(
                    '_paymentgateway/'.OperatorUrlizer::urlize($this).'?pgUuid='.base64_encode($this->uuid)
                );

                return new PaymentResponse($this, $internalRedirect);
        */
    }

    /**
     * @return CashUPayment
     */
    private function prepareCharge()
    {
        $this->getSettings();

        $charge = new CashUPayment();

        $charge->amount = $this->cart->getTotalGrossCost()->getAmountBasic();
        $charge->currency = $this->cart->getTotalGrossCost()->getCurrency()->getIsoCode();
        $charge->merchantId = $this->getSettings()->get('cashu.mechantId');
        $charge->testMode = $this->getSettings()->get('cashu.testMode');
        $charge->language = $this->getSettings()->get('cashu.language');
        $charge->serviceName = $this->getSettings()->get('cashu.serviceName');
        $charge->description = $this->paymentDetails->getDescription();
        $charge->hiddenDescription = $this->uuid;
        $charge->sessionId = $this->uuid;
        $charge->token = $this->prepareToken($charge->merchantId, $charge->amount, $charge->currency);

        return $charge;
    }

    /**
     * @param string        $merchantId
     * @param integer|float $amount
     * @param string        $currency
     *
     * @return string
     */
    private function prepareToken($merchantId, $amount, $currency)
    {
        $this->getSettings();
        $key = $this->getSettings()->get('cashu.encryptionKey');
        $values = [
            strtolower($merchantId),
            strtolower($amount),
            strtolower($currency),
            $key,
        ];

        $string = implode(':', $values);

        return md5($string);
    }

    /**
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processNotification(array $data)
    {
        $this->getSettings();
        // $thankYouPage = $this->getSettings()->get('cashu.thankyou_page');
        $dataType = $this->getResponseType($data);
        \Log::info('Got '.$dataType.' notification');
        if ($dataType == 'failure') {
            $this->cancel();
        }
        if ($dataType == 'xml') {
            $xmlResponse = new \SimpleXMLElement($data['sRequest']);
            if ($xmlResponse->responseCode == 'OK') {
                try {
                    \Log::info('Payment arrived and we want to accept it');
                    $this->accept();
                } catch (\Exception $ex) {
                    \Log::error($ex->getMessage());
                }
            } else {
                \Log::info('Response Code for XML notification was '.$xmlResponse->responseCode);
            }
        }

        return \Redirect::to($this->returnUrl);
        // return \Redirect::to($thankYouPage.'?hashid='.$this->hashId);
    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendRefundRequest()
    {
        // if no exception occured, we assume refund was created

        return new PaymentResponse($this, null);
    }

    /**
     * @param array $data
     *
     * @throws \RuntimeException
     * @return string
     */
    public static function extractUuid(array $data)
    {
        /* this section is really stupid. facts are that in case of failure, CashU doesn't
        send back the UUID of the payment, which we need to make the payment canceled.
        That's why we must use such damn stupid and error prone code. */
        if (isset($data['mode'])) {
            if ($data['mode'] == 'failure') {
                $uuid = \Session::get('payment_uuid');
                if($uuid){
                    \Session::forget('payment_uuid');
                    return $uuid;
                }
                return null;
            }
        }
        \Log::info(print_r($data, true));
        if (isset($data['session_id'])) {
            return $data['session_id'];
        } elseif (count($data) > 0) {
            try {
                $maybeXml = $data['sRequest'];
                $xmlResponse = new \SimpleXMLElement($maybeXml);

                return $xmlResponse->session_id;

            } catch (\Exception $e) {
                throw new \RuntimeException('Invalid redirect, payment uuid is missing.');
            }
        } else {
            throw new \RuntimeException('Invalid redirect, payment uuid is missing.');
        }
    }

    /**
     * @param mixed $data
     *
     * @return string
     */
    private function getResponseType($data)
    {
        if (isset($data['mode'])) {
            if ($data['mode'] == 'failure') {
                return 'failure';
            }
        }
        if (isset($data['session_id'])) {
            return 'array';
        } elseif (count($data) > 0 && isset($data['sRequest'])) {
            $maybeXml = $data['sRequest'];
            try {
                $xmlResponse = new \SimpleXMLElement($maybeXml);

                return 'xml';

            } catch (\Exception $e) {
                return 'error';
            }
        } else {
            return 'error';
        }
    }

}
